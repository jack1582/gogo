package gogo

import (
	"net/http"
	"strconv"
)

var (
	phttpRouterHandler *httpRouterHandler
)

func init() {
	if phttpRouterHandler == nil {
		phttpRouterHandler = &httpRouterHandler{}
	}
}

// GET 注册GET请求函数，注册固定URL、解析URL函数
// pattern string：触发路径
// handler httpProcFunc: 触发函数
func GET(pattern string, handler func(*HTTPContext)) {
	phttpRouterHandler.RegHTTPProcFunc("GET", pattern, handler)
}

// POST 注册POST请求函数，注册固定URL、解析URL函数
// pattern string：触发路径
// handler httpProcFunc: 触发函数
func POST(pattern string, handler func(*HTTPContext)) {
	phttpRouterHandler.RegHTTPProcFunc("POST", pattern, handler)
}

// PUT 注册PUT请求函数，注册固定URL、解析URL函数
// pattern string：触发路径
// handler httpProcFunc: 触发函数
func PUT(pattern string, handler func(*HTTPContext)) {
	phttpRouterHandler.RegHTTPProcFunc("PUT", pattern, handler)
}

// DELETE 注册DELETE请求函数，注册固定URL、解析URL函数
// pattern string：触发路径
// handler httpProcFunc: 触发函数
func DELETE(pattern string, handler func(*HTTPContext)) {
	phttpRouterHandler.RegHTTPProcFunc("DELETE", pattern, handler)
}

// STATUS 注册指定HTTP状态触发的函数
// status int：触发状态
// handler httpProcFunc: 触发函数
func STATUS(status int, handler func(*HTTPContext)) {
	phttpRouterHandler.RegHTTPStatusFunc(status, httpProcFunc(handler))
}

// PUBLIC 注册PUBLIC请求函数，可以共享文件查看下载
// pattern string：触发路径
// handler httpProcFunc: 触发函数
func PUBLIC(pattern, path string) {
	phttpRouterHandler.RegHTTPPublic(pattern, path)
}

// StartHTTP 启动HTTP服务
// gorouter.Run() default run on HttpPort
// gorouter.Run("localhost")
// gorouter.Run(":8089")
// gorouter.Run("127.0.0.1:8089")
func StartHTTP(port int) {
	addr := ":" + strconv.Itoa(port)
	LogInfo("HTTP Service Running on ", addr)
	CheckErrorExit(http.ListenAndServe(addr, phttpRouterHandler))
}

// 兼容http标准地址写法的，方便指定ip
func ListenAndServe(addr string) {
	LogInfo("HTTP Service Running on ", addr)
	CheckErrorExit(http.ListenAndServe(addr, phttpRouterHandler))
}