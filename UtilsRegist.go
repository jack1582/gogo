package gogo

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
)

type RecvJsonStruct struct {
	Online string `json:"online"`
	Msg    string `json:"msg"`
}

func Regist(regist_url string) {
	LogInfo("申请授权中")

	resp, err := http.Get(regist_url)
	CheckErrorExit(err)

	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	CheckErrorExit(err)

	var RecvJSON RecvJsonStruct
	err = json.Unmarshal(data, &RecvJSON)
	CheckErrorExit(err)

	LogInfo(RecvJSON.Msg)

	if RecvJSON.Online != "yes" {
		os.Exit(1)
	}
}
