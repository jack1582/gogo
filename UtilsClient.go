package gogo

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"
)

// ClientGet 发送GET请求
// url：        请求地址
// timeOut: 	请求超时时间
func ClientGet(url string, timeOut time.Duration) ([]byte, error) {
	client := &http.Client{Timeout: timeOut}
	resp, err := client.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	return ioutil.ReadAll(resp.Body)
}

// ClientPost 发送POST请求
// url：        请求地址
// data：       POST请求提交的数据
// contentType：请求体格式，如：application/json
// timeOut: 	请求超时时间，单位秒
func ClientPost(url string, data interface{}, contentType string, timeOut time.Duration) ([]byte, error) {
	client := &http.Client{Timeout: timeOut}
	jsonStr, _ := json.Marshal(data)
	resp, err := client.Post(url, contentType, bytes.NewBuffer(jsonStr))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	return ioutil.ReadAll(resp.Body)
}
