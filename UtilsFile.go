package gogo

import (
	"io/ioutil"
	"os"
)

func FileReadAll(fileName string) []byte {
	if fileName != "" {
		if fileHandle, err := os.Open(fileName); err == nil {
			defer fileHandle.Close()

			if fileBytes, err := ioutil.ReadAll(fileHandle); err == nil {
				return fileBytes
			} else {
				LogError(err.Error())
			}
		} else {
			LogError(err.Error())
		}
	} else {
		LogErrorF("FileReadAll: %s is null", fileName)
	}

	return nil
}

func FileMkdir(fileName string) bool {
	if fileName != "" {
		if _, err := os.Stat(fileName); err != nil {
			if err := os.MkdirAll(fileName, 0777); err == nil {
				return true
			} else {
				LogError(err.Error())
			}
		}
	} else {
		LogErrorF("FileMkdir: %s is null", fileName)
	}

	return false
}

func FileDel(fileName string) bool {
	if fileName != "" {
		if err := os.Remove(fileName); err == nil {
			return true
		} else {
			LogError(err.Error())
		}
	} else {
		LogErrorF("FileDel: %s is null", fileName)
	}

	return false
}

func FileAppend(fileName string, fileBytes []byte) bool {
	if fileName != "" {
		if fileBytes != nil {
			if fileHandle, err := os.OpenFile(fileName, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0660); err == nil {
				defer fileHandle.Close()
				if _, err := fileHandle.Write(fileBytes); err == nil {
					return true
				} else {
					LogError(err.Error())
				}
			} else {
				LogError(err.Error())
			}
		} else {
			LogErrorF("FileAppend: %s fileBytes is null", fileName)
		}
	} else {
		LogErrorF("FileAppend: %s is null", fileName)
	}

	return false
}

// 判断文件是否存在
func FileExist(fileName string) bool {
	if fileName != "" {
		// 读取文件信息，判断文件是否存在
		_, err := os.Stat(fileName)
		if err == nil {
			return true

		} else if os.IsExist(err) {
			return true
		}

	} else {
		LogErrorF("FileExist: %s is null", fileName)
	}

	return false
}
