package gogo

import (
	"fmt"
	"os"

	go_logger "github.com/phachon/go-logger"
)

var (
	pLogger *go_logger.Logger
)

// LogInit 初始化日志输出功能
// level 日志输出级别
// 		配置文件指定日志级别  ALL，DEBUG，INFO，WARN，ERROR，FATAL，OFF 级别由低到高
// 		其中 ALL表示所有调用打印日志的方法都会打出，而OFF则表示都不会打出。
// 		一般习惯是测试阶段为debug，生成环境为info以上
// maxSize 日志文件最大体积，单位MB
func LogInit(level string, maxSize int64) {
	if pLogger == nil {

		if _, err := os.Stat("log"); err != nil {
			os.Mkdir("log", 0777)
		}

		pLogger = go_logger.NewLogger()

		pLogger.Detach("console")

		// 命令行输出配置
		consoleConfig := &go_logger.ConsoleConfig{
			Color:      true,  // 命令行输出字符串是否显示颜色
			JsonFormat: false, // 命令行输出字符串是否格式化
			Format:     "",    // 如果输出的不是 json 字符串，JsonFormat: false, 自定义输出的格式
		}
		// 添加 console 为 logger 的一个输出
		pLogger.Attach("console", pLogger.LoggerLevel(level), consoleConfig)

		// 文件输出配置
		fileConfig := &go_logger.FileConfig{
			//Filename: "./log/test.log", // 日志输出文件名，不自动存在
			// 如果要将单独的日志分离为文件，请配置LealFrimeNem参数。
			LevelFileName: map[int]string{
				pLogger.LoggerLevel("error"): "./log/error.log", // Error 级别日志被写入 error .log 文件
				pLogger.LoggerLevel("info"):  "./log/info.log",  // Info 级别日志被写入到 info.log 文件中
				pLogger.LoggerLevel("debug"): "./log/debug.log", // Debug 级别日志被写入到 debug.log 文件中
			},
			MaxSize: maxSize * 1024, // 文件最大值（KB），默认值0不限
			//MaxLine:    100000, // 文件最大行数，默认 0 不限制
			DateSlice:  "d",   // 文件根据日期切分， 支持 "Y" (年), "m" (月), "d" (日), "H" (时), 默认 "no"， 不切分
			JsonFormat: false, // 写入文件的数据是否 json 格式化
			Format:     "",    // 如果写入文件的数据不 json 格式化，自定义日志格式
		}
		// 添加 file 为 logger 的一个输出
		pLogger.Attach("file", pLogger.LoggerLevel(level), fileConfig)

	} else {
		pLogger.Error("初始化失败，之前已经初始化过日志输出功能!")
	}
}

func logCheckInit() {
	if pLogger == nil {
		fmt.Println("gogo: 日志功能未经过初始化")
		os.Exit(1)
	}
}

// LogDebug 日志输出和官方fmt.Print、fmt.Printf使用一致
func LogDebug(a ...interface{}) {
	logCheckInit()

	if a[0] != nil {
		pLogger.Debug(fmt.Sprint(a...))
	}
}

// LogDebugF 日志输出和官方fmt.Print、fmt.Printf使用一致
func LogDebugF(format string, a ...interface{}) {
	logCheckInit()

	if a[0] != nil {
		pLogger.Debug(fmt.Sprintf(format, a...))
	}
}

// LogInfo 日志输出和官方fmt.Print、fmt.Printf使用一致
func LogInfo(a ...interface{}) {
	logCheckInit()

	if a[0] != nil {
		pLogger.Info(fmt.Sprint(a...))
	}
}

// LogInfoF 日志输出和官方fmt.Print、fmt.Printf使用一致
func LogInfoF(format string, a ...interface{}) {
	logCheckInit()

	if a[0] != nil {
		pLogger.Info(fmt.Sprintf(format, a...))
	}
}

// LogWarning 日志输出和官方fmt.Print、fmt.Printf使用一致
func LogWarning(a ...interface{}) {
	logCheckInit()

	if a[0] != nil {
		pLogger.Warning(fmt.Sprint(a...))
	}
}

// LogWarningF 日志输出和官方fmt.Print、fmt.Printf使用一致
func LogWarningF(format string, a ...interface{}) {
	logCheckInit()

	if a[0] != nil {
		pLogger.Warning(fmt.Sprintf(format, a...))
	}
}

// LogError 日志输出和官方fmt.Print、fmt.Printf使用一致
func LogError(a ...interface{}) {
	logCheckInit()

	if a[0] != nil {
		pLogger.Error(fmt.Sprint(a...))
	}
}

// LogErrorF 日志输出和官方fmt.Print、fmt.Printf使用一致
func LogErrorF(format string, a ...interface{}) {
	logCheckInit()

	if a[0] != nil {
		pLogger.Error(fmt.Sprintf(format, a...))
	}
}
