package gogo

import "reflect"

type TaskModel struct {
	mEndRunning chan int
	mQueue      chan interface{}
	mTaskClass  interface{}
}

func NewTask(taskClass interface{}, count int) *TaskModel {
	r := &TaskModel{make(chan int), make(chan interface{}, count), taskClass}
	r.Start()
	return r
}

func (c *TaskModel) Release() {
	for range c.mEndRunning {
	}

	val := reflect.ValueOf(c.mTaskClass)
	tFunc := val.MethodByName("Release")
	args := make([]reflect.Value, 0)
	tFunc.Call(args)
}

func (c *TaskModel) Start() {
	go func() {
		val := reflect.ValueOf(c.mTaskClass)
		tFunc := val.MethodByName("Proc")
		args := make([]reflect.Value, 1)

		for data := range c.mQueue {
			args[0] = reflect.ValueOf(data)
			tFunc.Call(args)
		}

		close(c.mEndRunning)
	}()
}

func (c *TaskModel) Push(data interface{}) error {
	c.mQueue <- data
	return nil
}

func (c *TaskModel) SendRelease() {
	close(c.mQueue)
}
