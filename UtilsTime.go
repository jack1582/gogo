package gogo

import (
	"fmt"
	"time"
)

func TimeGetNowStr() (time_str string) {
	now := time.Now()
	time_str = fmt.Sprintf("%02d/%02d/%02d %02d:%02d:%02d\n", now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), now.Second())
	time_str = time_str[:len(time_str)-2] //-2是为了去掉时间后面的回车符/n
	return
}

func TimeSleepSecond(sec int) {
	time.Sleep(TimeIntToDuration(sec))
}

func TimeIntToDuration(sec int) time.Duration {
	return time.Duration(sec) * time.Second
}
