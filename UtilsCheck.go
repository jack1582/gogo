package gogo

import (
	"os"
)

func CheckErrorExit(err ...interface{}) {
	if err[0] != nil {
		LogError(err)
		os.Exit(1)
	}
}
