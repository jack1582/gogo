package gogo

var (
	mGlobalSignal chan int
)

// GlobalSignalWait 全局信号等待
// 需要程序等待协程运行的时候使用
func GlobalSignalWait() {
	for range mGlobalSignal {
	}
}

// GlobalSignalRelease 全局信号继续
// 需要程序不再等待协程运行的时候使用
func GlobalSignalRelease() {
	close(mGlobalSignal)
}
